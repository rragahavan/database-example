package com.example.emaildatabaseapplication.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.emaildatabaseapplication.Model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 17-11-2016.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "zuniDatabase";
    private static final String TABLE_CONTACTS = "contacts";
    private static final String USER_ID = "id";
    //private static final String USER_NAME = "name";
    private static final String USER_EMAIL = "email";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        /*String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_CONTACTS + "("
                + USER_ID + " INTEGER PRIMARY KEY," + USER_NAME + " TEXT,"
                + USER_EMAIL + " TEXT" + ")";*/

        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_CONTACTS + "("
                + USER_ID + " INTEGER PRIMARY KEY,"
                + USER_EMAIL + " TEXT" + ")";


        db.execSQL(CREATE_CONTACTS_TABLE);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);
        onCreate(db);
    }

    public void addContact(User contact) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        //values.put(USER_NAME, contact.getName());
        values.put(USER_EMAIL, contact.getEmailId());


        db.insert(TABLE_CONTACTS, null, values);
        db.close();
    }

    User getContact(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_CONTACTS, new String[]{USER_ID,
                        USER_EMAIL}, USER_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        User contact = new User(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1));

        return contact;
    }


    public List<User> getAllContacts() {
        List<User> contactList = new ArrayList<User>();

        String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        if (cursor.moveToFirst()) {
            do {
                User contact = new User();
                contact.set_id(Integer.parseInt(cursor.getString(0)));
                //contact.setName(cursor.getString(1));
                contact.setEmailId(cursor.getString(1));
                contactList.add(contact);
            } while (cursor.moveToNext());
        }
        return contactList;
    }

}