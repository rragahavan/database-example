package com.example.emaildatabaseapplication.Model;

public class User {

    int _id;
    //private String name;
    private String emailId;
	/*public User(int _id, String name, String emailId)*/
    public User(int _id,String emailId)

    {
        this._id = _id;
        //this.name = name;
        this.emailId = emailId;
    }

    //public User(String name, String emailId) {
    public User(String emailId){
        //this.name = name;
        this.emailId = emailId;
    }

    public User() {

    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }


    /*public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
*/	public String getEmailId() {
        return emailId;
    }
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }
}