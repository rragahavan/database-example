package com.example.emaildatabaseapplication.View;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.emaildatabaseapplication.Adapter.UserAdapter;
import com.example.emaildatabaseapplication.Database.DatabaseHandler;
import com.example.emaildatabaseapplication.Model.User;
import com.example.emaildatabaseapplication.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class ResultActivity extends AppCompatActivity {
    private Button submitButton;
    private Button submitBtn;
    private EditText nameEdtTxt, emailEdtTxt;
    private JSONObject userObject;
    String name,email;
    ListView emailListView;
    List<User> contacts;
    ArrayList<String> emailListItems=new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        emailEdtTxt = (EditText)findViewById(R.id.email_edttxt);
        emailListView=(ListView)findViewById(R.id.emailList);
        submitBtn = (Button)findViewById(R.id.submit_btn);
        submitBtn.setOnClickListener(onSubmitClickListener);


    }

    protected View.OnClickListener onSubmitClickListener = new View.OnClickListener() {


        @Override
        public void onClick(View v) {
            User user = new User();
           // name = nameEdtTxt.getText().toString().trim();
            email = emailEdtTxt.getText().toString().trim();
            emailListItems.clear();
            Boolean isValid=false;
            /*appContext.getUserObject().setName(name);
            appContext.getUserObject().setEmailId(email);
*/
            try {
                userObject = new JSONObject();

                if (email != null) {
                    if (!email.equals("") || email.length() != 0) {
                        if (android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                            userObject.put("email", email);
                            user.setEmailId(email);
                            isValid = true;
                        }
                    }
                }

                System.out.println("USER : " + userObject);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            //storing to local database
            if (isValid) {
                DatabaseHandler db = new DatabaseHandler(ResultActivity.this);
                Log.d("Insert: ", "Inserting ..");
                db.addContact(user);
                // post to server
                /*HttpAsyncTask httpAsyncTask = new HttpAsyncTask(UserDetailsActivity.this, userObject, UserDetailsActivity.this);
                httpAsyncTask.execute(AppConstants.SERVER_END_POINT + AppConstants.POST_USER_API);*/
                /*Intent nxtIntent = new Intent(UserDetailsActivity.this, Question1.class);
                startActivity(nxtIntent);
*/
                Log.d("Reading: ", "Reading all contacts..");
                contacts = db.getAllContacts();

                for (User users : contacts) {
                    String log = "Id: " + users.get_id() + " ,Email: " + users.getEmailId();
                    // Writing Contacts to log
                    Log.d("Name: ", log);
                    emailListItems.add(users.getEmailId());
                }
                            } else {
                        /*userObject.put("email", "");
                        user.setEmailId("");*/
                Toast.makeText(ResultActivity.this, getResources().getString(R.string.enter_valid_email),
                        Toast.LENGTH_LONG).show();
            }
            UserAdapter adapater=new UserAdapter(ResultActivity.this,emailListItems);
            emailListView.setAdapter(adapater);

        }
    };
}
