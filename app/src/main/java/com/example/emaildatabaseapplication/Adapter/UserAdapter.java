package com.example.emaildatabaseapplication.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.emaildatabaseapplication.R;

import java.util.ArrayList;

/**
 * Created by Administrator on 17-11-2016.
 */
public class UserAdapter extends BaseAdapter {
    private LayoutInflater layoutInflater;
    public ArrayList<String> list;
    public UserAdapter(Activity activity, ArrayList<String> list) {

        layoutInflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.list=list;
    }

    @Override
    public int getCount() {

        return this.list.size();

    }

    @Override
    public Object getItem(int position) {

        return position;
    }

    @Override
    public long getItemId(int position) {

        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View listItem = convertView;
        int pos = position;
        if (listItem == null) {
            listItem = layoutInflater.inflate(R.layout.email_list, null);
        }

        TextView userEmail = (TextView) listItem.findViewById(R.id.userEmail);
        userEmail.setText(this.list.get(pos));
        return listItem;
    }

}